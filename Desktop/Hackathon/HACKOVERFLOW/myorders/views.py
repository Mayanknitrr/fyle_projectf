from django.shortcuts import render
from .models import Order,Delivery
from cart.models import Cart
from user_panel.models import Review,Customer
from django.http import HttpResponse, HttpResponseRedirect,JsonResponse
from products.models import ProductData
from django.views.decorators.csrf import csrf_exempt

def index(request):
	orders = Order.objects.filter(customer__user_id=request.user.id)
	return render(request,'myorders/index.html',{ "orders":orders })

def detail(request,bill_id):
	order = Order.objects.get(bill=bill_id)
	cart = order.cart.all()
	try:
		delivery = Delivery.objects.get(order__bill=bill_id)
	except:
		return render(request,'myorders/detail.html',{ "order":order,"cart":cart,'delivery':[],'msg':"Not yet dispactched" })

	return render(request,'myorders/detail.html',{ "order":order,"cart":cart,'delivery':delivery })

@csrf_exempt
def review(request,p_id):
	if request.method =='GET':
		return render(request,'myorders/review.html')
	else:
		try:
			review = Review()
			review.description = request.POST.get('review',False)	
			customer = Customer.objects.get(user_id=request.user.id)
			review.customer_id = customer.id
			review.save()
			product = ProductData.objects.get(id=p_id)
			product.reviews.add(review)
			product.save()
			return HttpResponse('success')
		except:
			return HttpResponse('error')



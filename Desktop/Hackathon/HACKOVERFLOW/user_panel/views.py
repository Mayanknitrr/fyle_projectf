from django.shortcuts import render
from translator import get_in_hindi,get_utube
import wikipedia
from django.http import HttpResponse, HttpResponseRedirect,JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from . models import Customer
from django.contrib.auth.models import User
from django.contrib.auth import login as auth_login


def translator(request):
	
	print(request.user)
	text = "1. Organic manures produce optimal condition in the soil for high yields and good quality crops2. They supply the entire nutrient required by the plant (NPK, secondary and micronutrients).3. They improve plant growth and physiological activities of plants"

	text = get_in_hindi(text)
	return render(request,'user_panel/translate.html',{ "text":text })



@csrf_exempt
def chatbot(request):
	if request.method == "GET":
		print(str(request.user.email))
		customer = Customer.objects.get(email=str(request.user.email))
		return render(request,'user_panel/chatbot.html',{'customer':customer})
	else:
		print("post request wikipeida")
		query_obj = json.loads(request.body.decode('utf-8'))
		text = str(query_obj['query'])
		url = get_utube(text)
		results = {}
		try:
			results = wikipedia.summary(text,sentences=5)
			results = { "english":results,"hindi":get_in_hindi(results),"url":url }
		except:
			print("wikipedia failed to saerch")
			results["english"] = "Not found anything"
			results["hindi"] = get_in_hindi(results["english"])
			results["url"] = url

		
		return JsonResponse(results)




